#!/bin/bash

# Source the profile because on librelec all the binaries are in weird locations
source /etc/profile

off=0x00
low=0x01
med=0x21
high=0x41 
full=0x64

debug="true"

function get_temp() {
	data=$(cat /sys/class/thermal/thermal_zone0/temp)
	degree=$(echo $data | cut -b 0-2)
	dot=$(echo $data | cut -b 3-9)
	echo $degree
}

# Ensure that we always get the right device. This is mainly a hack around 
# i2c and upon a cold boot it sometimes gets a different id... Possbily not,
# but it never hurts to be through
function get_device() {
	[[ -e /dev/i2c-0 ]] && device=0 
	[[ -e /dev/i2c-1 ]] && device=1 
	[[ -e /dev/i2c-2 ]] && device=2 
	echo $device
}

function set_fan() {
	status=$1
	device=$(get_device)
	if [[ $status == "off" ]]; then i2cset -y $device 0x01a $off;
	elif [[ $status == "low" ]]; then i2cset -y $device 0x01a $low;
	elif [[ $status == "med" ]]; then i2cset -y $device 0x01a $med;
	elif [[ $status == "high" ]]; then i2cset -y $device 0x01a $high;
	else i2cset -y $device 0x01a $full;
	fi
}

while true; do
	currenttemp=$(get_temp)
	if [[ $currenttemp -le 45 ]]; then
		set_fan "off";
	elif [[ $currenttemp -le 54 ]]; then 
		set_fan "low";
	elif [[ $currenttemp -le 57 ]]; then 
	       	set_fan "med";
	elif [[ $currenttemp -le 65 ]]; then 
	       	set_fan "high";
	elif [[ $currenttemp -gt 65 ]]; then 
		set_fan "full";
	fi
	
    # This is mainly for systemd output which is standard for my setups
	[[ $debug = "true" ]] && echo "Temp: $currenttemp"
	sleep 60
done

