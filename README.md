sh implementation using i2cset binary to set the fan speed of the ArgonOne case. 
Compatible with LibreElec and any other distribution that using the i2cset binary.

## Probable Requirements:

On LibreElec you probably need the Addons:
- System-Tools
- rpi-tools

On other distributions, install the `i2c-tools` package via your package manager.
Or just run `raspi-config` and turn on i2c if you are on Raspberry OS / Rasbian.

Reboot for good measure

----

Download the file to your system: 
and run: ./fancontrol.sh

